"use strict";

new fullpage('#fullpage', {
  autoScrolling: true,
  navigation: true,
  navigationPosition: 'left',
  // responsiveWidth: 400,
  scrollingSpeed: 500,
  anchors: ['intro-1', 'intro-2', 'intro-3', 'intro-4', 'intro-5', 'intro-6', 'intro-7', 'home-1', 'home-2', 'home-3', 'home-4', 'testimonial-1', 'testimonial-2', 'testimonial-3', 'our-partnerships', 'news-media', 'contact-us'],
  onLeave: function onLeave(origin, destination, direction) {
    var introOverlayIntro = document.querySelector('.intro-overlay-intro');
    var introOverlayInnerContent = document.querySelector('.intro-overlay .inner-content');
    var navbar = document.querySelector('.navbar');
    var footer = document.querySelector('.footer');
    var scrollNav = document.querySelector('.scroll-nav');

    if (destination.index > 0) {
      introOverlayIntro.classList.add('hide-element-opacity');

      if (destination.index > 6) {
        introOverlayInnerContent.classList.add('hide-element-translate');
        scrollNav.classList.add('hide-element-translate-opacity');
        navbar.classList.remove('hide-element-opacity'); // fullpage_api.setResponsive(true);
      } else {
        introOverlayInnerContent.classList.remove('hide-element-translate');
        scrollNav.classList.remove('hide-element-translate-opacity');
        navbar.classList.add('hide-element-opacity'); // fullpage_api.setResponsive(false);
      }
    } else {
      introOverlayIntro.classList.remove('hide-element-opacity');
    }

    if (destination.anchor === 'contact-us') {
      footer.classList.remove('hide-element-translate-opacity');
    } else {
      footer.classList.add('hide-element-translate-opacity');
    }

    if (destination.anchor === 'intro-1') {
      scrollNav.classList.remove('hide-scroll-down');
      scrollNav.classList.remove('hide-element-translate-opacity');
    } else {
      scrollNav.classList.add('hide-scroll-down');
    }
  }
});
var burger = document.querySelector('.navbar-burger');
var nav = document.querySelector('.navbar-links');
var navLinks = document.querySelectorAll('.navbar-links li');
var scrollNav = document.querySelector('.scroll-nav');
var scrollDown = document.querySelector('.scroll-nav .scroll-down');
var skipIntro = document.querySelector('.scroll-nav .skip-intro');
var downArrow = document.querySelector('.scroll-nav .down-arrow');

function handleClick() {
  // Toggle navbar.
  nav.classList.toggle('navbar-links-active'); // Animate links.

  navLinks.forEach(function (link, index) {
    if (link.style.animation) {
      link.style.animation = '';
    } else {
      link.style.animation = "navbarLinksFade .4s ease-out forwards ".concat(index / 18 + .05, "s");
    }
  }); // Animate burger.

  burger.classList.toggle('animate');
}

burger.addEventListener('click', function () {
  handleClick();
});
navLinks.forEach(function (link, index) {
  link.addEventListener('click', function () {
    handleClick();
  });
});
scrollDown.addEventListener('click', function () {
  fullpage_api.moveSectionDown();
});
skipIntro.addEventListener('click', function () {
  fullpage_api.moveTo(8);
});
downArrow.addEventListener('click', function () {
  fullpage_api.moveSectionDown();
});